package com.danny.mystock.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.danny.mystock.R;
import com.danny.mystock.Utils.DialogAddStock;

import static com.danny.mystock.Utils.DialogAddStock.REQUEST_CROP_PHOTO;
import static com.danny.mystock.Utils.DialogAddStock.REQUEST_LOAD_PHOTO;
import static com.danny.mystock.Utils.DialogAddStock.REQUEST_STORAGE;
import static com.danny.mystock.Utils.DialogAddStock.REQUEST_TAKE_PHOTO;

public class MainActivity extends BaseActivity {

    private DialogAddStock dialogAddStock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getData();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add_stock:
                addStock();
                break;
            case R.id.btn_search_stock:
                searchStock();
                break;
            case R.id.btn_view_stock:
                viewStock();
                break;
        }
    }

    private void viewStock() {
        startActivity(new Intent(MainActivity.this, ViewStockActivity.class));
    }

    private void searchStock() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_search_stock);
    }

    private void addStock() {
        dialogAddStock = DialogAddStock.getInstance(MainActivity.this);
        dialogAddStock.addStock();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dialogAddStock.pickImage();
            } else {
                Toast.makeText(this, "Storage permission wan not granted!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_LOAD_PHOTO:
                if (resultCode == RESULT_OK) {
                    if (data != null && data.getData() != null) {
                        dialogAddStock.setmImageCaptureUri(data.getData());
                        dialogAddStock.doCrop();
                    }
                }
                break;

            case REQUEST_TAKE_PHOTO:
                if (resultCode == RESULT_OK && dialogAddStock.getmImageCaptureUri() != null) {
                    dialogAddStock.doCrop();
                }
                break;

            case REQUEST_CROP_PHOTO:
                if (resultCode == RESULT_OK) {
                    dialogAddStock.logo.setImageURI(data.getData());
                }
                break;
            default:
                break;
        }
    }


}
