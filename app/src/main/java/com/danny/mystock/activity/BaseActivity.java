package com.danny.mystock.activity;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.test.espresso.core.deps.guava.collect.Lists;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Danny on 9/13/2017.
 */

public class BaseActivity extends AppCompatActivity implements ValueEventListener {
    public ProgressDialog progressDialog;
    public StorageReference storageReference;
    private List<DataSnapshot> allItems = new ArrayList<>();

    public List<DataSnapshot> getAllItems() {
        return allItems;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(this);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.getWindow().setDimAmount(0);
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setGravity(Gravity.CENTER);


        storageReference = FirebaseStorage.getInstance().getReference();
    }

    public void getData() {
        FirebaseDatabase.getInstance().getReference("datas").addValueEventListener(this);
    }

    public DatabaseReference getDataReference() {
        return FirebaseDatabase.getInstance().getReference("datas");
    }

    public DatabaseReference getCategoriesReference() {
        return FirebaseDatabase.getInstance().getReference("categories");
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        allItems = Lists.newArrayList(dataSnapshot.getChildren());
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }
}
