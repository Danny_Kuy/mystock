package com.danny.mystock.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.test.espresso.core.deps.guava.collect.Lists;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.danny.mystock.R;
import com.danny.mystock.adapter.ItemsAdapter;
import com.danny.mystock.callback.RecyclerViewCallback;
import com.google.firebase.database.DataSnapshot;

/**
 * Created by Danny on 9/11/2017.
 */

public class ViewStockActivity extends BaseActivity {
    private RecyclerView recyclerView;
    private ItemsAdapter itemsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_stock);
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        itemsAdapter = new ItemsAdapter(new RecyclerViewCallback() {
            @Override
            public void onItemClickListener(int id) {
            }
        });
        itemsAdapter.addItems(getAllItems());
        recyclerView.setAdapter(itemsAdapter);
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        itemsAdapter.addItems(Lists.newArrayList(dataSnapshot.getChildren()));
    }
}
