package com.danny.mystock.callback;

/**
 * Created by Danny on 9/12/2017.
 */

public interface RecyclerViewCallback {
    void onItemClickListener(int id);
}
