package com.danny.mystock.adapter;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.danny.mystock.R;
import com.danny.mystock.callback.RecyclerViewCallback;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Danny on 9/11/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<DataSnapshot> dataSnapshots = new ArrayList<>();
    private RecyclerViewCallback recyclerViewCallback;

    public CategoryAdapter(RecyclerViewCallback recyclerViewCallback) {
        this.recyclerViewCallback = recyclerViewCallback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_category, parent, false));
    }

    public void addItems(List<DataSnapshot> dataSnapshots) {
        this.dataSnapshots.clear();
        this.dataSnapshots.addAll(dataSnapshots);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CategoryViewHolder)
            ((CategoryViewHolder) holder).bind();
    }

    @Override
    public int getItemCount() {
        return dataSnapshots.size();
    }

    private class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView more;

        CategoryViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.title);
            more = (ImageView) itemView.findViewById(R.id.more);
        }

        void bind() {
            final int id = dataSnapshots.get(getAdapterPosition()).child("id").getValue(Integer.class);
            name.setText(dataSnapshots.get(getAdapterPosition()).child("name").getValue(String.class));
            more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popupMenu = new PopupMenu(itemView.getContext(), v);
                    popupMenu.inflate(R.menu.more_menu);
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.update:
//                                    ((BaseActivity) itemView.getContext())
//                                            .getDatabaseReference("categories/" + dataSnapshots.get(getAdapterPosition()).getKey())
//                                            .setValue(dataSnapshots);
                                    Toast.makeText(itemView.getContext(), "Update", Toast.LENGTH_SHORT).show();
                                    return true;
                                case R.id.delete:
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(itemView.getContext());
                                    builder.setMessage(R.string.make_sure);
                                    builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
//                                            ((BaseActivity) itemView.getContext()).getDatabaseReference("categories/" + dataSnapshots.get(getAdapterPosition()).getKey()).removeValue();
//                                            ((BaseActivity) itemView.getContext()).getDatabaseReference("datas/" + id).removeValue();
                                        }
                                    });
                                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                                    builder.create().show();
                                    return true;
                            }
                            return false;
                        }
                    });
                    popupMenu.show();
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    recyclerViewCallback.onItemClickListener(id);
                }
            });
        }
    }
}
