package com.danny.mystock.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.danny.mystock.R;
import com.danny.mystock.callback.RecyclerViewCallback;
import com.google.firebase.database.DataSnapshot;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Danny on 9/11/2017.
 */

public class ItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<DataSnapshot> dataSnapshots = new ArrayList<>();
    private RecyclerViewCallback recyclerViewCallback;

    public ItemsAdapter(RecyclerViewCallback recyclerViewCallback) {
        this.recyclerViewCallback = recyclerViewCallback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_items, parent, false));
    }

    public void addItems(List<DataSnapshot> dataSnapshots) {
        this.dataSnapshots.clear();
        this.dataSnapshots.addAll(dataSnapshots);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemsViewHolder)
            ((ItemsViewHolder) holder).bind();
    }

    @Override
    public int getItemCount() {
        return dataSnapshots.size();
    }

    private class ItemsViewHolder extends RecyclerView.ViewHolder {
        TextView name, id, price, qty;
        ImageView logo;

        ItemsViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.title);
            price = (TextView) itemView.findViewById(R.id.price);
            qty = (TextView) itemView.findViewById(R.id.qty);
            id = (TextView) itemView.findViewById(R.id.id);
            logo = (ImageView) itemView.findViewById(R.id.logo);
        }

        void bind() {
            DataSnapshot dataSnapshot = dataSnapshots.get(getAdapterPosition());
            name.setText(dataSnapshot.child("name").getValue(String.class));
            id.setText(String.format(Locale.ENGLISH, "%s - %d", itemView.getContext().getString(R.string.string_id),
                    dataSnapshot.child("id").getValue(Integer.class)));
            DecimalFormat df = new DecimalFormat("###.##", new DecimalFormatSymbols());
            price.setText(String.format(Locale.ENGLISH, "%s %s", df.format(dataSnapshot.child("price").getValue(Double.class)),
                    dataSnapshot.child("currency").getValue(String.class).equals("usd") ? "$" : "៛"));
            qty.setText(String.format(Locale.ENGLISH, "%d %s", dataSnapshot.child("qty").getValue(Integer.class), itemView.getContext().getString(R.string.left)));
            Glide.with(itemView.getContext()).load(dataSnapshot.child("logo").getValue(String.class)).placeholder(R.drawable.ic_machine).error(R.drawable.ic_machine).into(logo);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    recyclerViewCallback.onItemClickListener(0);
                }
            });
        }
    }
}
