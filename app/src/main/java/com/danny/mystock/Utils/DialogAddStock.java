package com.danny.mystock.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.test.espresso.core.deps.guava.collect.Lists;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.danny.mystock.BuildConfig;
import com.danny.mystock.R;
import com.danny.mystock.activity.BaseActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Danny on 9/13/2017.
 */

public class DialogAddStock {
    private static DialogAddStock INSTANCE;
    private Context c;
    public static final int REQUEST_STORAGE = 400;
    public static final int REQUEST_LOAD_PHOTO = 4;
    public static final int REQUEST_TAKE_PHOTO = 5;
    public static final int REQUEST_CROP_PHOTO = 44;
    private Uri mImageCaptureUri;
    private File mOutputFile;
    public ImageView logo;
    private Dialog dialog;
    private ArrayList<String> categories = new ArrayList<>();
    private ArrayList<Integer> categoriesId = new ArrayList<>();
    private int categoryId;

    public static DialogAddStock getInstance(Context context) {
        return INSTANCE != null ? INSTANCE : new DialogAddStock(context);
    }

    private DialogAddStock(Context context) {
        c = context;
        mOutputFile = new File(this.c.getExternalFilesDir(null).getAbsolutePath(), "image_crop_tmp.jpg");
    }

    public void addStock() {
        mImageCaptureUri = null;
        final ArrayAdapter<String> aa = new ArrayAdapter<>(c, android.R.layout.simple_spinner_item);
        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        final View view = LayoutInflater.from(c).inflate(R.layout.dialog_add_stock, null);
        builder.setView(view);
        final Spinner spin = (Spinner) view.findViewById(R.id.spinner);
        final RadioButton rdButtonUS = (RadioButton) view.findViewById(R.id.usd);
        final RadioButton rdButtonKH = (RadioButton) view.findViewById(R.id.riel);
        logo = (ImageView) view.findViewById(R.id.logo);
        view.findViewById(R.id.cam).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
                String imageFileName = "IMG_" + timeStamp + ".jpg";
                File imageFile;
                try {
                    imageFile = File.createTempFile("IMG_" + timeStamp, ".jpg", c.getExternalFilesDir(Environment.DIRECTORY_PICTURES));
                    mImageCaptureUri = FileProvider.getUriForFile(c, BuildConfig.APPLICATION_ID + ".provider", imageFile);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                    intent.putExtra("listPhotoName", imageFileName);
                    ((Activity) c).startActivityForResult(intent, REQUEST_TAKE_PHOTO);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        view.findViewById(R.id.gal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });
        ((BaseActivity) c).getCategoriesReference().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<DataSnapshot> dataSnapshots = Lists.newArrayList(dataSnapshot.getChildren());
                categories = new ArrayList<>();
                for (int i = 0; i < dataSnapshots.size(); i++) {
                    categories.add(dataSnapshots.get(i).child("name").getValue(String.class));
                    categoriesId.add(dataSnapshots.get(i).child("id").getValue(Integer.class));
                }
                aa.clear();
                aa.addAll(categories);
                aa.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                categoryId = categoriesId.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        view.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.btn_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) c).progressDialog.show();
                Item item = new Item();
                item.setId(((BaseActivity) c).getAllItems().size() + 1);
                item.setCurrency(rdButtonUS.isChecked() ? String.valueOf(rdButtonUS.getTag()) : String.valueOf(rdButtonKH.getTag()));
                item.setName(((EditText) view.findViewById(R.id.name)).getText().toString());
                item.setQty(((EditText) view.findViewById(R.id.qty)).getText().toString());
                item.setPrice(Integer.parseInt(((EditText) view.findViewById(R.id.price)).getText().toString()));
                item.setCategoryId(categoryId);
                onAdd(item);
            }
        });
        spin.setAdapter(aa);
        dialog = builder.create();
        dialog.show();
    }

    private void onAdd(final Item item) {
        Uri uri = Uri.fromFile(mOutputFile);
        ((BaseActivity) c).storageReference.child("img").child(uri.getLastPathSegment()).putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                item.setLogo(String.valueOf(taskSnapshot.getDownloadUrl()));
                ((BaseActivity) c).getDataReference().push().setValue(item).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(c, "Success add event.", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        ((BaseActivity) c).progressDialog.dismiss();
                    }
                });
            }
        });
    }

    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        ((Activity) c).startActivityForResult(intent, REQUEST_LOAD_PHOTO);
    }


    public void doCrop() {
        try {
            mOutputFile = File.createTempFile("crop_" + Calendar.getInstance().getTimeInMillis() + "_", ".jpg", c.getExternalFilesDir(Environment.DIRECTORY_PICTURES));
            Uri mImageCropUri = FileProvider.getUriForFile(c, BuildConfig.APPLICATION_ID + ".provider", mOutputFile);
            Intent intent = new Intent("com.android.camera.action.CROP");
            intent.setDataAndType(mImageCaptureUri, "image/*");
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            intent.putExtra("scale", true);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCropUri);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            List<ResolveInfo> resInfoList = c.getPackageManager().queryIntentActivities(intent, 0);
            for (int i = 0; i < resInfoList.size(); i++) {
                if (resInfoList.get(i) != null) {
                    String packageName = resInfoList.get(i).activityInfo.packageName;
                    if ("com.google.android.apps.photos".equals(packageName)) {
                        intent.setComponent(new ComponentName(packageName, resInfoList.get(i).activityInfo.name));
                    }
                    c.grantUriPermission(packageName, mImageCropUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
            }
            ((Activity) c).startActivityForResult(intent, REQUEST_CROP_PHOTO);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Uri getmImageCaptureUri() {
        return mImageCaptureUri;
    }

    public void setmImageCaptureUri(Uri mImageCaptureUri) {
        this.mImageCaptureUri = mImageCaptureUri;
    }

    private class Item {
        private String currency;
        private String name;
        private String logo;
        private String qty;
        private int id;
        private int price;
        private int categoryId;

        public int getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(int categoryId) {
            this.categoryId = categoryId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }
    }
}
